import { useState, useEffect } from 'react'
import axios from 'axios';
import Items from './components/Items/items';
import './App.css';

function App() {

  const [itemText, setItemText] = useState('');
  const [listItems, setListItems] = useState([]);
  const [isUpdating, setIsUpdating] = useState('');
  const [updateItemText, setUpdateItemText] = useState('');
  //const [listItemDetails, getListItemDetails] = useState([]);

  //add new todo item to database
  const addItem = async (e) => {
    e.preventDefault();
    try {
      const res = await axios.post('http://localhost:5500/api/item', { item: itemText });
      //console.log(res);
      setListItems(prev => [...prev, res.data]);
      setItemText('');
    } catch (err) {
      console.log(err);
    }
  }
  //Create function to fetch all todo items from database -- we will use useEffect hook

  useEffect(() => {
    const getItemsList = async () => {
      try {
        const res = await axios.get('http://localhost:5500/api/items');
        //console.log(res.data);
        setListItems(res.data);
      } catch (err) {
        console.log(err);
      }
    }
    getItemsList();

  }, []);
  //Create function to fetch all details item from database -- we will use useEffect hook
 /*  useEffect(() => {
    const getItemDetails = async (id) => {
      try {
        const res = await axios.get(`http://localhost:5500/api/item/${id}`);
        //console.log(res.data);
        getListItemDetails(res.data);
      } catch (err) {
        console.log(err);
      }
    }
    getItemDetails();

  }, []); */
  //delete item when click on delete
  const deleleItem = async (id) => {
    try {
      const res = await axios.delete(`http://localhost:5500/api/item/${id}`)
      console.log(res.data);
      const newListItems = listItems.filter(item => item._id !== id);
      setListItems(newListItems);
    } catch (err) {
      console.log(err);
    }
  }
  //Update item
  const updateItem = async (e) => {
    e.preventDefault()
    try {
      const res = await axios.put(`http://localhost:5500/api/item/${isUpdating}`, { item: updateItemText })
      console.log(res.data);
      const updatedItemIndex = listItems.findIndex(item => item._id === isUpdating);
      const updatedItem = listItems[updatedItemIndex].item = updateItemText;
      setUpdateItemText('');
      setIsUpdating('');
    } catch (err) {
      console.log(err);
    }
  }
  //Display details of task
/*   const renderDisplayForm = () => (
    <form className='display-details'>
      <table>
        <tr>
          <th>Assigned to</th>
          <th>Priority</th>
          <th>Date</th>
        </tr>
        <tr>
          <td>Abdo</td>
          <td>high</td>
          <td>02/05/2024</td>
        </tr>
      </table>
    </form>
  ) */
  //before updating item we need to show input field where we will create our updated item
  const renderUpdateForm = () => (
    <form className="update-form" onSubmit={(e) => { updateItem(e) }}>
      <input className="update-new-input" type="text" placeholder="add new item" onChange={e => { setUpdateItemText(e.target.value) }} value={updateItemText} />
      <button className="update-new-btn" type="submit">Update</button>
    </form>
  )
  return (
    <div className="App">
      <h1>Todo List</h1>
      <form className="form" onSubmit={e => addItem(e)}>
        <input className="task" type="text" placeholder='Add todo item' onChange={e => { setItemText(e.target.value) }} value={itemText} />
        {/* <input className="assigned" type="text" placeholder='Assigned to' onChange={e => { setItemText(e.target.value) }} value={itemText} /> */}
        <select className='custom-select'>
          <option value='0' >Assigned to</option>
          <option>Mohammed</option>
          <option>Zineb</option>
          <option>Anouar</option>
        </select>
        <button type="submit">Add</button>
      </form>
      <div className="todo-listItems">
        {
          listItems.map(item => (
            <div className="todo-item" key={item._id}>
              {
                isUpdating === item._id
                  ? renderUpdateForm()
                  : <>
                    <p className="item-content">{item.item}</p>
                    <button className="details-item" onClick={() => { setIsUpdating(item._id) }}>Details</button>
                    <button className="update-item" onClick={() => { setIsUpdating(item._id) }}>Update</button>
                    <button className="delete-item" onClick={() => { deleleItem(item._id) }}>Delete</button>
                  </>
              }

            </div>
          ))
        }
       
      </div>   
      <div className='to_left'>
        <Items />
      </div>   
    </div>
  
  );
}

export default App;
